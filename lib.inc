%define READ_SYSCALL 0
%define WRITE_SYSCALL 1
%define EXIT_SYSCALL 60
%define STDIN 0
%define STDOUT 1
%define ASCII_SPACE 0x20
%define ASCII_TAB 0x9
%define ASCII_NEWLINE 0xA
%define ASCII_ZERO 48
%define ASCII_NINE 57
%define ASCII_MINUS 45



section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length ; получаем длину строки в rax
    mov rdx, rax ; длина в rdx
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    pop rsi
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    mov rax, WRITE_SYSCALL
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ASCII_NEWLINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, 10
    mov r8, 1
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
    .loop:
        xor rdx, rdx
        inc r8
        div r9
        add rdx, ASCII_ZERO
        dec rsp
        mov byte[rsp], dl
        test rax, rax ; исправлено
        jnz .loop
    .end:
        mov rdi, rsp
        push r8
        call print_string
        pop r8
        add rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; исправлено?
    jge .pos
    .neg:
        push rdi
        mov rdi, ASCII_MINUS
        call print_char
        pop rdi
        neg rdi
    .pos:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop
        mov al, [rdi + rcx]
        cmp al, [rsi + rcx]
        jne .not
        test al, al ; исправлено
        jz .ok
        inc rcx
        jmp .loop
    .ok
        mov rax, 1
        ret
    .not
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, READ_SYSCALL
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .loop:
        call read_char
        test rax, rax
        jz .ok
        cmp rax, ASCII_SPACE
        je .skip
        cmp rax, ASCII_TAB
        je .skip
        cmp rax, ASCII_NEWLINE
        je .skip
        cmp r13, r14
        jle .fail
        mov [r12 + r14], rax
        inc r14
        jmp .loop
    .skip:
        test r14, r14 ; исправлено
        jz .loop
    .ok:
        mov byte[r12 + r14], 0
        mov rax, r12
        mov rdx, r14
        jmp .end
    .fail:
        xor rax, rax
    .end:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r9, 10
    .loop:
        mov r8b, byte[rdi + rdx]
        cmp r8b, ASCII_ZERO
        jb .end
        cmp r8b, ASCII_NINE
        ja .end
        push rdx
        mul r9
        pop rdx
        sub r8, ASCII_ZERO
        add rax, r8
        inc rdx
        jmp .loop
    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], ASCII_MINUS
    je .neg
    call parse_uint
    jmp .end
    .neg:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        jge .err
        mov rax, [rdi + rcx]
        mov [rsi + rcx], rax
        inc rcx
        test rax, rax ; исправлено
        jz .end
        jmp .loop
    .err:
        xor rax, rax
        ret
    .end:
        mov rax, rcx
        ret
